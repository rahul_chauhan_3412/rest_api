const homecontroller = require('../controller/home.controller');


exports.routesConfig= function(app){
 
    app.get('/home',[
        homecontroller.gethome
    ]);

    app.post('/api/home/add',[
        homecontroller.home
    ]);

    app.get('/api/get-user-by-id/:id',[
        homecontroller.getUserById
    ])

    app.put('/api/home/user-update',[
        homecontroller.userUpdate
    ])

    app.delete('/api/home/delete',[
        homecontroller.delete
    ])
    app.get('/user/upload', [
        homecontroller.uploadfile
    ]); 
    
   
}