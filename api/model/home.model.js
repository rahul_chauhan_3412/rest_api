const mongoose = require('../../common/services/mongoose.service').mongoose;


const Schema = mongoose.Schema;

const homeSchema= new Schema({
    fullname:{type:String},
    email:{type:String},
    username:{type:String},
     gender:{type:String},
    state:{type:String},
     city:{type:String}, 
    createdAt: {type: String},
    fullUrl:{type:String}


});

const home= mongoose.model('tbl_jobtest',homeSchema)


exports.savehome = (Data) => {
    const homeSave = new home({
        fullname:Data.fullname,
        email:Data.email,
        username:Data.username,
        gender:Data.gender,
        state:Data.state,
        city:Data.city, 
        fullUrl:Data.fullUrl

    });

    return homeSave.save();
};

exports.saveImage = (img) => {
    console.log(img)
     const homeSave = new home({
         img : img,
         
     });
     return homeSave.save();
 
 };

exports.gethome = () => {
    return new Promise((resolve,reject)=>{
        home.find({})
            .exec(function(err,home){
                if(err){
                    reject(err);
                }else{
                    resolve(home);
                }

            });

      });
}



exports.getUserById = (id) =>{
    return new Promise((resolve,reject) => {
        home.find({_id:id})
        .exec(function(err,home){
            if(err){
                reject(err);
            }else{
                resolve(home);
            }
        })
    });

}

exports.updateUser = (data) => {
    console.log(data)
    return new Promise((resolve, reject) => {
        home.updateOne({_id:data.user_id},{fullname:data.fullname_update,email:data.email_update,username:data.username_update,
            gender:data.gender_update,state:data.state_update,city:data.city_update})
            .exec(function(err,home){
                if(err){
                    reject(err);
                }else{
                    resolve(home);
                }

            })
    });
};



exports. deleteUser=(id)=>{
    return new Promise((resolve,reject)=>{
        home.deleteOne({_id:id})
        .exec(function (err,home){
            if(err){
                reject(err);
            }else{
                resolve(home);
            }
        })
    
    })
    }